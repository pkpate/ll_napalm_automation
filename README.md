# Lunch and Learn -- NAPALM Automation

[[_TOC_]]

## Installation

> Clone the [repository](https://gitlab.com/pkpate/ll_napalm_automation) from Gitlab.  Setup a Python virtual environment, activate it, and install requirements.  

```bash
# Clone the Repo
git clone https://gitlab.com/pkpate/ll_napalm_automation.git

# Change into root of repository
cd ll_napalm_automation

# Create and activate Python Virtual Environment (MacOS or Linux)
python3 -m venv venv
source venv/bin/activate

# Create and activate Python Virtual Environment (Windows)
py -3 -m venv venv
venv/Scripts/activate

# Install requirements
pip install -r requirements.txt
```



## Usage

1. Copy env.template to .env

```bash
$ cp env.template .env
```

2. Open .env and update `WEBEX_TEAMS_ACCESS_TOKEN` and `WEBEX_TEAMS_ROOM_ID`
   * Update `WEBEX_TEAMS_ACCESS_TOKEN` by getting your *Personal Access Token* from [https://developer.webex.com/docs/api/getting-started](https://developer.webex.com/docs/api/getting-started)
   * Update `WEBEX_TEAMS_ROOM_ID` with the Room ID provided by instructor in your Webex Teams Room
