"""This script connects to the NX-OS Always-On Sandbox, captures running config, and writes it to a file."""

# Import required modules
import napalm
import os
from datetime import datetime
from dotenv import load_dotenv
from webexteamssdk import WebexTeamsAPI, exceptions

# Load Environment Variables defined in .env file
load_dotenv()
NXOS_SANDBOX = os.getenv("NXOS_SANDBOX")
SSH_PORT = os.getenv("SSH_PORT")
USERNAME = os.getenv("NXOS_USERNAME")
PASSWORD = os.getenv("NXOS_PASSWORD")
WT_ACCESS_TOKEN = os.getenv("WEBEX_TEAMS_ACCESS_TOKEN")
WT_ROOM_ID = os.getenv("WEBEX_TEAMS_ROOM_ID")


def get_running_config(device_ip, device_user, device_password, device_port=22):
    """Connect to device and capture running config"""

    # Use the appropriate network driver to connect to the device
    driver = napalm.get_network_driver('nxos_ssh')

    # Define device object to connect to
    device = driver(hostname=device_ip,
                    username=device_user,
                    password=device_password,
                    optional_args={'port': device_port})

    # Connect to device and capture running config
    print(f'Connecting to {device_ip}...')
    with device as d:
        print(f'Capturing running config...')
        configs = d.get_config(retrieve='running')

    return configs


def write_running_config(hostname, running_config):
    """Write running config to file"""

    # Define filename using timestamp and hostname
    timestamp = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
    filename = f'{hostname}_run_conf_{timestamp}.txt'

    # Create directory named 'running_configs' if it doesn't exist
    if not os.path.exists('running_configs'):
        os.makedirs('running_configs')

    # Write running config to file
    with open(f'running_configs/{filename}', 'w') as file:
        file.write(running_config)
    print(f'Running configuration written to "{filename}"')

    return filename


def post_message(message, filepath):
    """Post message in Webex Teams Room upon successful completion of exercise"""

    # Post message in Webex Teams room and attach any files
    try:
        teamsapi = WebexTeamsAPI(access_token=WT_ACCESS_TOKEN)
        print('Posting message to Webex Teams Room...')
        teamsapi.messages.create(WT_ROOM_ID, text=message, files=[filepath])
    except exceptions.AccessTokenError:
        print('Please ensure an unexpired \'WEBEX_TEAMS_ACCESS_TOKEN\''
              'is correctly populated in .env file')
    except exceptions.ApiError:
        print('Please ensure \'WEBEX_TEAMS_ROOM_ID\''
              'is correctly populated in .env file')


if __name__ == '__main__':
    configs = get_running_config(device_ip=NXOS_SANDBOX,
                                 device_user=USERNAME,
                                 device_password=PASSWORD,
                                 device_port=SSH_PORT)
    filename = write_running_config(hostname=NXOS_SANDBOX,
                                    running_config=configs['running'])
    if os.path.isfile(f'running_configs/{filename}'):
        msg = f'I successfully gathered this running config from {NXOS_SANDBOX}!'
        post_message(message=msg, filepath=f'running_configs/{filename}')
