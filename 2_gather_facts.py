"""This script connects to the NX-OS Always-On Sandbox, gathers facts, and posts them in a Webex Teams Room."""

# Import required modules
import napalm
import os
from dotenv import load_dotenv
from webexteamssdk import WebexTeamsAPI, exceptions

# Load Environment Variables defined in .env file
load_dotenv()
NXOS_SANDBOX = os.getenv("NXOS_SANDBOX")
SSH_PORT = os.getenv("SSH_PORT")
USERNAME = os.getenv("NXOS_USERNAME")
PASSWORD = os.getenv("NXOS_PASSWORD")
WT_ACCESS_TOKEN = os.getenv("WEBEX_TEAMS_ACCESS_TOKEN")
WT_ROOM_ID = os.getenv("WEBEX_TEAMS_ROOM_ID")


def gather_facts(device_ip, device_user, device_password, device_port=22):
    """Connect to device and gather facts"""

    # Use the appropriate network driver to connect to the device
    driver = napalm.get_network_driver('nxos_ssh')

    # Define device object to connect to
    device = driver(hostname=device_ip,
                    username=device_user,
                    password=device_password,
                    optional_args={'port': device_port})

    # Connect to device and gather facts
    print(f'Connecting to {device_ip}...')
    with device as d:
        print(f'Gathering facts...')
        facts = d.get_facts()

    return facts


def parse_facts(facts: dict):
    """Parse facts to extract hostname, serial number, and os version"""

    # TODO: Examine the contents of the facts dictionary.  Then parse through
    # it to find the hostname, serial number, and os version of the device.

    hostname =  # Enter Code Here
    os =  # Enter Code Here
    serial_no =  # Enter Code Here

    # TODO:  Loop through the interface_list and print the names of only loopback interfaces

    return hostname, os, serial_no


def post_message(message):
    """Post message in Webex Teams Room upon successful completion of exercise"""

    # Post message in Webex Teams room
    try:
        teamsapi = WebexTeamsAPI(access_token=WT_ACCESS_TOKEN)
        print('Posting message to Webex Teams Room...')
        teamsapi.messages.create(WT_ROOM_ID, text=message)
    except exceptions.AccessTokenError:
        print('Please ensure an unexpired \'WEBEX_TEAMS_ACCESS_TOKEN\''
              'is correctly populated in .env file')
    except exceptions.ApiError:
        print('Please ensure \'WEBEX_TEAMS_ROOM_ID\''
              'is correctly populated in .env file')


if __name__ == '__main__':
    nxos_ao_sbx_facts = gather_facts(device_ip=NXOS_SANDBOX,
                                     device_user=USERNAME,
                                     device_password=PASSWORD,
                                     device_port=SSH_PORT)
    hostname, os, serial_no = parse_facts(nxos_ao_sbx_facts)
    if '9.3' in os and 'sbx-n9kv' in hostname and '9QX' in serial_no:
        msg = f'''I successfully parsed facts on {NXOS_SANDBOX}!
                  hostname = {hostname}
                  os version = {os}
                  serial number = {serial_no}'''
        post_message(message=msg)
